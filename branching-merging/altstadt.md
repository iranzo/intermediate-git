# Altstadt

### Soban

Zwinsgerstr. 21, Heidelberg, BW 69117

https://www.restaurant-soban.de/speisekarte


#### Pro

- good, spicy Korean food

#### Con

- not many vegetarian/vegan options
- quite small - best to make a reservation!
