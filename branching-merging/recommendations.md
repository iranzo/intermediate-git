# Restaurant recommendations

A colection of recommended restaurants in/near Heidelberg.

Click on the links below to see recommendations for a particular area.

- [Rohrbach](rohrbach.md)
- [Bergheim](bergheim.md)
- [Altstadt](altstadt.md)


#### Contributors

- Toby Hodges

Contributions are encouraged! 
Please read the instructions in [CONTRIBUTING.md](CONTRIBUTING.md) before submitting a Merge Request.
