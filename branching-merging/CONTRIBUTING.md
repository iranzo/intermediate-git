# Guidance for contributors

__Thank you for your interest in contributing to my awesome project!__

Recommendation files should take the following structure:

```markdown
# <Area Name>

### <Restaurant Name>

<address>

<website URL>

#### Pro

- list of
- good things

#### Con

- list of
- bad things

### <Restaurant Name 2>

...
```

When adding a new file,
please make sure to add a link to that file in `recommendations.md`.
And add your name to the list of contributors while you're there, too!